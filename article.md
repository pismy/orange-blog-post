---
title: "How Orange made a first step towards CI/CD standardization"
author: Pierre Smeyers
author_gitlab: pismy
author_twitter: pismy
categories: open source
image_title: 'tbc-card.png'
description: "GitLab CI/CD success story from customer: Orange"
tags: cicd, DevOps, open source, GitLab CI
---

# How Orange made a first step towards CI/CD standardization

## Introduction

CI/CD is a foundational piece to modern software development. It's a major brick in the DevOps "Automation" pillar and every company involved in IT has to implement CI/CD or they’re already quite far behind the curve. 

But implementing CI/CD involves a few challenges, especially in growing or large companies:

* DevOps expertise & technical skills,
* DevSecOps,
* Standardization.

In this article, I'll detail each of those challenges and tell you how [Orange](http://orange.com/) overcame them with GitLab.

## DevOps and technical skills

No matter which CI/CD tool you're using, it requires some amount of expertise to implement it right.

**DevOps expertise** first because - well - you need to have some experience (and some strong opinions too) with Git workflows, deployment, environments, secrets management, etc. You can't ask a complete rookie to implement a state-of-the art DevOps pipeline without expertise or experience.

**Technical skills** also play a part with the CI/CD tool you are using.
As professionals, we're never satisfied with _getting started tutorials_, are we? We inevitably need advanced functions, and that requires knowing the tool pretty well.
This is particularly true with GitLab CI/CD, which is a fantastic tool, but functionally very rich, and also constantly evolving, adding a recurring burden for projects that want to integrate new functionalities as they go.

## DevSecOps

DevOps is all about finding the right balance between **shortening the cycle** and **maximizing your confidence** (in being/feeling _in control_).

DevSecOps tools are a key stone in maximizing our confidence, as they help us detect issues almost instantaneously (security, code quality, compliance, ...).

But DevSecOps tools are evolving at a very rapid pace and today's Docker container scanner tools can be replaced by newcomers in just a few months.

Delegating the choice and integration of the (right) DevSecOps tools to every single development team in your company is a nonsense, as they won't have time to spend on this non-functional topic. If you do, what will most probably happen is that your developers **won't use any** DevSecOps tool because they feel the opportunity cost isn't worth the time and effort.

## Standardization

The last challenge in implementing CI/CD at the scale of a large company is the lack of standardization.

GitLab CI/CD - as most other CI/CD tools - is mainly a sophisticated scheduler, allowing to define technical tasks and their sequence, but GitLab CI/CD cares little about the nature of these tasks, and does not give any clues as to the "right" way to build a DevOps pipeline. The consequence of this is that every company, every project team, every single developer will implement his DevOps pipeline in his own way, probably significantly different from the others.

As a lifelong Javaist, I like to compare the current situation in CI/CD with what was the Java build in the pre-Maven era. Back then, we used non-structuring tools such as [Make](https://en.wikipedia.org/wiki/Make_(software)) or [Apache Ant](https://en.wikipedia.org/wiki/Apache_Ant), each project built up its own build system, adopted its own conventions, its own code and resource files structure, … in short, it was a happy mess, everyone reinvented the wheel, and when joining another project, it was necessary to relearn "how does the build work here?".
Then in 2004, Maven was released (and Gradle 3 years later). For a while there were heated debates between the proponents of standardization and the defenders of expertise and of "I can control how it works", but today it would not occur to anyone to build a Java project other than with Maven or Gradle. If I join a project developed in Java, I will immediately know how files are organized, how the project is built. To summarize: the Java build has become a non-topic. Standard is good.

I believe this is what must happen now in the field of CI/CD: tools shall offer a more opinionated framework so that the CI/CD in turn becomes a non-topic.

## How a single GitLab feature changed the game for Orange

At Orange - probably like many other companies involved in IT - we struggled with the 3 above challenges.

Then in January 2019, the [`include`](https://docs.gitlab.com/ee/ci/yaml/index.html#include) feature was released in the Community Edition (version 11.7.0):

```yaml
include:
  - project: a-path/to-some-project'
    file: '/very-smart-template.yml'
```

We could suddenly develop and share state-of-the-art GitLab CI/CD pipeline templates!

And that's what we did.

During 2 years, a handful of DevOps/security/languages/cloud experts developed ready-to -use GitLab CI/CD pipeline templates. This personal initiative quickly became recognized as an internal project, attracting more and more users and contributors, bringing the community to 1000+ members today (June 2021), and leveraging about 30 available templates.
The visible effect of this increasing adoption is the beginning of a **CI/CD standardization at Orange**.

And guess what!? We were so happy with this and so convinced that it's a general need that we open sourced our templates under the name _"to be continuous"_.

[![tbc-card](https://to-be-continuous.gitlab.io/doc/img/tbc-card.png)](https://to-be-continuous.gitlab.io/doc/)

## What is in “to be continuous”? How can I start?

For now, _to be continuous_ has 26 templates of 6 kinds:

* **Build & Test**: Angular, Bash, Go, Gradle, Maven, MkDocs, Node.js, PHP, Python
* **Code Analysis**: Gitleaks, SonarQube
* **Packaging**: Docker
* **Infrastructure** (IaC): Terraform
* **Deploy & Run**: Ansible, Cloud Foundry, Google Cloud, Helm, Kubernetes, OpenShift, S3 (Simple Storage Service)
* **Acceptance**: Cypress, Postman, Puppeteer, Robot Framework, SSL test, k6
* **Others**: semantic-release

_To be continuous_ is thoroughly documented ([basic notions and philosophy](https://to-be-continuous.gitlab.io/doc/understand/), [general usage principles](https://to-be-continuous.gitlab.io/doc/usage/), every template also has [its own documentation](https://to-be-continuous.gitlab.io/doc/ref/angular/), how to use _to be continuous_ in a [self-managed GitLab](https://to-be-continuous.gitlab.io/doc/self-managed/basic/)).

To get started quickly, _to be continuous_ provides an [interactive configurer](https://to-be-continuous.gitlab.io//kicker/) (a.k.a “kicker”) that allows generating the `.gitlab-ci.yml` file simply by selecting the technical characteristics of your project.

Finally, _to be continuous_ exposes several [example projects](https://gitlab.com/to-be-continuous/samples), illustrating how to use the templates in production-like projects, combining multiple templates.

## A quick glance

As said above, there are tons of resources to get started with _to be continuous_. But here is a quick example to get the taste of it ;)

Here is the `.gitlab-ci.yml` file for a project:

* developed in Java 11 (built with Maven),
* code analysis with SonarQube,
* packaged as a Docker image,
* deployed to Kubernetes cluster,
* GUI tests with Cypress,
* API tests with Postman (Newman).

```yaml
include:
  # Maven template
  - project: "to-be-continuous/maven"
    ref: "1.4.2"
    file: "templates/gitlab-ci-maven.yml"
  # Docker template
  - project: "to-be-continuous/docker"
    ref: "1.2.0"
    file: "templates/gitlab-ci-docker.yml"
  # Kubernetes template
  - project: "to-be-continuous/kubernetes"
    ref: "1.2.0"
    file: "templates/gitlab-ci-k8s.yml"
  # Cypress template
  - project: "to-be-continuous/cypress"
    ref: "1.2.0"
    file: "templates/gitlab-ci-cypress.yml"
  # Postman template
  - project: "to-be-continuous/postman"
    ref: "1.2.0"
    file: "templates/gitlab-ci-postman.yml"

# Global variables
variables:
  # Explicitly define the Maven + JDK version
  MAVEN_IMAGE: "maven:3.8-openjdk-11"

  # Enables SonarQube analysis (on sonarcloud.io)
  SONAR_URL: "https://sonarcloud.io"
  # organization & projectKey defined in pom.xml
  # SONAR_AUTH_TOKEN defined as a secret CI/CD variable

  # Kubernetes
  K8S_KUBECTL_IMAGE: "bitnami/kubectl:1.17" # client version matching my cluster
  K8S_URL: "https://k8s-api.my.domain" # Kubernetes Cluster API url
  # K8S_CA_CERT & K8S_TOKEN defined as secret CI/CD variables
  # enable review, staging & prod
  K8S_REVIEW_SPACE: "non-prod"
  K8S_STAGING_SPACE: "non-prod"
  K8S_PROD_SPACE: "prod"

  # Cypress & Postman: enable test on review aps
  REVIEW_ENABLED: "true"

# Pipeline steps
stages:
  - build
  - test
  - package-build
  - package-test
  - review
  - staging
  - deploy
  - acceptance
  - publish
  - production
```

This fully declarative file produces the following **development pipeline** (any feature branch):

![development pipeline](development-pipeline.png)

… and the following **production pipeline** (`master` or `main` depending on your preferences):

![production pipeline](production-pipeline.png)

Although they look pretty much the same, they aren’t:

* While the production pipeline privileges sureness and completeness, development pipelines privilege short cycles and developer experience. As a result, while code analysis jobs and acceptance tests are blocking in production, they only generate a non-blocking warning in development in case of failure.
* The production pipeline first deploys to the staging environment before deploying to production (provided acceptance tests are green); development pipelines may deploy to a dynamically generated review environment (optional).
* Developers may prefer using a single integration environment (associated to the develop branch) instead or one review app per feature branch. The default behavior of the integration pipeline is much closer to the production one.

What you can't see:

* Java unit tests are automatically executed, their report is [integrated to GitLab](https://docs.gitlab.com/ee/ci/unit_test_reports.html), with [code coverage](https://docs.gitlab.com/ee/ci/yaml/#coverage) too.
* SonarQube integration automatically uses [branch analysis](https://docs.sonarqube.org/latest/branches/overview/) or [MR analysis](https://docs.sonarqube.org/latest/analysis/pull-request/) (with MR decoration) depending on the context.
* Kubernetes environments are obviously [integrated to GitLab](https://docs.gitlab.com/ee/ci/environments/) too.
* [Review apps](https://docs.gitlab.com/ee/ci/review_apps/index.html) can be cleaned-up manually or automatically on branch deletion.
* Cypress and Postman tests reports are also [integrated to GitLab](https://docs.gitlab.com/ee/ci/unit_test_reports.html).
* Docker uses Kaniko build by default but might be configured to use Docker-in-Docker instead. It uses the GitLab registry by default but can be configured to use any other registry.
* Each template integrates the most appropriate DevSecOps tools ([kube-score](https://kube-score.com/) for Kubernetes, [hadolint](https://github.com/hadolint/hadolint) for Docker, [OWASP Dependency-Check](https://jeremylong.github.io/DependencyCheck/) for Maven, and others...).
* All those templates combine themselves gracefully. Examples: Kubernetes may simply deploy the Docker image built upstream, whichever the branch; Cypress and Postman tests automatically test the application deployed in the upstream jobs, whichever the branch; Kubernetes could be replaced with OpenShift, GCP or any other supported hosting technology: it would behave the same.

## Conclusion

[to be continuous](https://to-be-continuous.gitlab.io/doc) is out and eagerly waiting for users and contributors!

Have a look at it and feel free to contribute.
You like it? Good. Use it and let us know.
Do you disagree with our choices? Good too. Your inputs are even more valuable to help us improve _to be continuous_ and cover as many use cases as possible.

But anyway, never forget this: [`include`](https://docs.gitlab.com/ee/ci/yaml/index.html#include) is undoubtedly the feature that makes CI/CD standardization possible in your company (and beyond)!
